import React, { useState, useEffect } from 'react'
import { GoogleMap, useJsApiLoader  } from '@react-google-maps/api'
import DefaultSpinner from './SpinnerComp'
import Stack from 'react-bootstrap/Stack';
import { Button, InputGroup, FormControl,
    Row, Col,
    SplitButton, Dropdown,
    Card, Container   } from 'react-bootstrap';
import { FaBars, FaSearch  } from 'react-icons/fa';
import Offcanvas from 'react-bootstrap/Offcanvas';
import Accordion from 'react-bootstrap/Accordion';
import ListGroup from 'react-bootstrap/ListGroup';
import { BsArrowRepeat } from 'react-icons/bs';
import { FaSave } from 'react-icons/fa';
import { FaLock, FaUnlock } from 'react-icons/fa';
import { IconContext } from 'react-icons';


import air from '../img/pinpoint/poi/air.png';
import suppliers from '../img/pinpoint/bu/suppliers.png';

const GOOGLE_API_KEY = "AIzaSyCcrSI5N1E60E8NTZhsoi0Z61wS7om8wnQ"

const containerStyle = {
  width: '100%',
  height: '100vh'
}

const defaultCenter = {
  lat: 34.0522, 
  lng: -118.2437
}

const mapOptions = {
  disableDefaultUI: true,
  zoomControl: true,
  fullscreenControl: true,
  mapTypeId: 'roadmap' // 'roadmap', 'satellite', 'hybrid', 'terrain'
}

const imageList = [
    air,
    air,
    air,
    air,
    air,
    air,
    air,
    air,
    air,
    air
  ];

  const units1 = [
    {name: 'Apply Extra Filters', img: air},
    {name: 'Reset Extra Filters', img: air},
  ];
  const units2 = [
    {name: 'Buildings', img: air},
    {name: 'ΙΔΙΟΚΤΗΤΟ', img: air},
    {name: 'ΕΝΟΙΚΙΑΖΟΜΕΝΟ', img: air},
    {name: 'Critical', img: air}
  ];
  const units3 = [
    {name: 'Shops', img: air}
  ];
  const units4 = [
    {name: 'KV', img: air}
  ];
  const units5 = [
    {name: 'Base Station', img: air}
  ];

  const unitsList = [units1, units2, units3, units4, units5];

  const poisList1 = [
    {name: 'Αεροδρόμια', img: air},
    {name: 'Αστυνομία', img: air},
    {name: 'Ασφαλιστικά', img: air},
    {name: 'ΑΤΜ', img: air},
    {name: 'Δημαρχεία', img: air},
    {name: 'Αεροδρόμια', img: air},
    {name: 'Αστυνομία', img: air},
    {name: 'Ασφαλιστικά', img: air},
    {name: 'ΑΤΜ', img: air},
    {name: 'Δημαρχεία', img: air},
    {name: 'Αεροδρόμια', img: air},
    {name: 'Αστυνομία', img: air},
    {name: 'Ασφαλιστικά', img: air},
    {name: 'ΑΤΜ', img: air},
    {name: 'Ασφαλιστικά', img: air},
    {name: 'ΑΤΜ', img: air},
    {name: 'Δημαρχεία', img: air}
  ];

const poisList2 = [
    {name: 'Supliers', img: suppliers},
    {name: 'Critical', img: suppliers},
    {name: 'Non Critical', img: suppliers},
    {name: 'Supplies Product', img: suppliers},
    {name: 'Supplies Service', img: suppliers}
  ];

const GoogleMapContainer = () => {
    const [isLocked, setIsLocked] = useState(true);
    const { isLoaded, loadError } = useJsApiLoader({
    id: 'google-map-script',
    libraries: ['places'],
    googleMapsApiKey: GOOGLE_API_KEY
    // ...otherOptions
    })

    const handleClick = () => {
        // Handle button click event here
        console.log('Button clicked');
      };



      const alertClicked = () => {
        alert('You clicked the third ListGroupItem');
      };


    const [show, setShow] = useState(false);

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);
    const renderMenu = () => {
        return (<>
        <Row style={{
                            position: 'absolute',
                            top: '20px',
                            left: '20px',
                            zIndex: 999
                        }}>
            <Col xs="auto">
            <Button 
                        variant="light"
                        onClick={handleShow}
                    >
                        <FaBars />
            </Button>
            </Col>
            <Col xs="auto">
            <InputGroup className="ml-3">
                <FormControl placeholder="Search" />
                <SplitButton
                    variant="primary"
                    title="Search"
                    id="segmented-button-dropdown-2"
                    alignRight
                    >
                    <Dropdown.Item href="#">Search</Dropdown.Item>
                    <Dropdown.Item href="#">Another action</Dropdown.Item>
                    <Dropdown.Item href="#">Something else here</Dropdown.Item>
                    <Dropdown.Divider />
                    <Dropdown.Item href="#">Separated link</Dropdown.Item>
                </SplitButton>
            </InputGroup>
            </Col>
            <Col xs="auto">
            <InputGroup className="ml-3">
                <Button 
                    variant="warning"
                    className="d-flex align-items-center">
                <BsArrowRepeat 
                    className="animate-spin me-2" 
                    style={{ fontSize: '1rem' }}
                />
                Load
                </Button>                
            </InputGroup>
            </Col>
            <Col xs="auto">
            <InputGroup className="ml-3">
                <Button variant="primary" className="d-flex align-items-center">
                    <FaSave className="me-2" />
                    Image
                </Button>         
            </InputGroup>
            </Col>
            <Col xs="auto">
            <InputGroup className="ml-3">
                <Button
                    variant={isLocked ? 'danger' : 'success'}
                    className="d-flex align-items-center"
                    onClick={() => setIsLocked(!isLocked)}
                    >
                    {isLocked ? <FaLock className="me-2" /> : <FaUnlock className="me-2" />}
                    {isLocked ? 'Lock' : 'Unlock'}
                </Button>         
            </InputGroup>
            </Col>
        </Row>

            <Offcanvas show={show} onHide={handleClose} style={{ maxWidth: '800px', width: '90%' }}>
                <Offcanvas.Header closeButton>
                <Offcanvas.Title>Menu</Offcanvas.Title>
                </Offcanvas.Header>
                <Offcanvas.Body>
                    <Accordion>
                        <Accordion.Item eventKey="0">
                            <Accordion.Header>Units</Accordion.Header>
                            <Accordion.Body>
                                {unitsList.map((items) => (
                                    <Container>
                                        <Row className="mt-1">
                                            {items.map((item) => (
                                                <Col key={''} xs={12} sm={12} md={3} lg={3}>
                                                    <Card>
                                                    <Button size="sm" variant="outline-primary" style={{ width: '100%', height: '100%' }}>
                                                        <div style={{ display: 'flex', flexDirection: 'row', alignItems: 'center' }}>
                                                        <Card.Img variant="top" src={item.img} alt="Image" style={{ width: '30px', objectFit: 'cover' }} />
                                                        <div style={{ display: 'flex', flexDirection: 'column', marginLeft: '4px' }}>
                                                            <small style={{ fontSize: '0.8rem' }}>{item.name}</small>
                                                        </div>
                                                        </div>
                                                    </Button>
                                                    </Card>
                                                </Col>
                                            ))}
                                        </Row>
                                    </Container>
                                ))}
                            </Accordion.Body>
                        </Accordion.Item>
                        <Accordion.Item eventKey="1">
                            <Accordion.Header>POIs</Accordion.Header>
                            <Accordion.Body>
                                <Container>
                                    <Row className="justify-content-between">
                                        {poisList1.map((item) => (
                                        <Col key={''} xs={12} sm={12} md={3} lg={3}>
                                            <Card>
                                            <Button size="sm" variant="outline-primary" style={{ width: '100%', height: '100%' }}>
                                                <div style={{ display: 'flex', flexDirection: 'row', alignItems: 'center' }}>
                                                <Card.Img variant="top" src={item.img} alt="Image" style={{ width: '30px', objectFit: 'cover' }} />
                                                <div style={{ display: 'flex', flexDirection: 'column', marginLeft: '4px' }}>
                                                    <small style={{ fontSize: '0.8rem' }}>{item.name}</small>
                                                </div>
                                                </div>
                                            </Button>
                                            </Card>
                                        </Col>
                                        ))}
                                    </Row>
                                </Container>
                            </Accordion.Body>
                            <Accordion.Body>
                                <Container>
                                    <Row className="justify-content-between">
                                        {poisList2.map((item, index) => (
                                        <Col key={index} xs={12} sm={12} md={3} lg={3}>
                                            <Card>
                                            <Button variant="outline-success" style={{ width: '100%', height: '100%' }}>
                                                <div style={{ display: 'flex', flexDirection: 'row', alignItems: 'center' }}>
                                                <Card.Img variant="top" src={item.img} alt="Image" style={{ width: '30px', objectFit: 'cover' }} />
                                                <div style={{ display: 'flex', flexDirection: 'column', marginLeft: '10px' }}>
                                                    <small style={{ fontSize: '0.8rem' }}>{item.name}</small>
                                                </div>
                                                </div>
                                            </Button>
                                            </Card>
                                        </Col>
                                        ))}
                                    </Row>
                                </Container>
                            </Accordion.Body>


                        </Accordion.Item>
                        <Accordion.Item eventKey="2">
                            <Accordion.Header>Clear</Accordion.Header>
                            <Accordion.Body className="p-1">
                                <ListGroup >
                                    <ListGroup.Item action onClick={alertClicked}>
                                        Selection
                                    </ListGroup.Item>
                                    <ListGroup.Item action onClick={alertClicked}>
                                        All
                                    </ListGroup.Item>
                                </ListGroup>
                            </Accordion.Body>
                        </Accordion.Item>
                        <Accordion.Item eventKey="3">
                            <Accordion.Header>Tools</Accordion.Header>
                            <Accordion.Body className="p-1">
                                <ListGroup >
                                    <ListGroup.Item action onClick={alertClicked}>
                                        Move Mode
                                    </ListGroup.Item>
                                    <ListGroup.Item action onClick={alertClicked}>
                                        Rectangle
                                    </ListGroup.Item>
                                    <ListGroup.Item action onClick={alertClicked}>
                                        Circle
                                    </ListGroup.Item>
                                    <ListGroup.Item action onClick={alertClicked}>
                                        Polygon
                                    </ListGroup.Item>
                                    <ListGroup.Item action onClick={alertClicked}>
                                        Traffic
                                    </ListGroup.Item>
                                    <ListGroup.Item action onClick={alertClicked}>
                                        Roadmap
                                    </ListGroup.Item>
                                    <ListGroup.Item action onClick={alertClicked}>
                                        Satellite
                                    </ListGroup.Item>
                                    <ListGroup.Item action onClick={alertClicked}>
                                        View BCM Incidents
                                    </ListGroup.Item>
                                    <ListGroup.Item action onClick={alertClicked}>
                                        View Physical Security Incidents
                                    </ListGroup.Item>
                                    <ListGroup.Item action onClick={alertClicked}>
                                        Get Directions
                                    </ListGroup.Item>
                                </ListGroup>
                            </Accordion.Body>
                        </Accordion.Item>
                    </Accordion>
                </Offcanvas.Body>
            </Offcanvas>
        </>)
    }

    const renderMap = () => {
        return (
            <Stack gap={3}>
                <GoogleMap
                    options={mapOptions}
                    mapContainerStyle={containerStyle}
                    zoom={10}
                    center={defaultCenter}
                >
                {renderMenu()}
                </GoogleMap>
            </Stack>
        )
    }

    if (loadError) {
    return <div>Map cannot be loaded right now, sorry.</div>
    }

    return isLoaded ? (renderMap()) : DefaultSpinner()
}

// export default React.memo(GoogleMapContainer)
export default GoogleMapContainer