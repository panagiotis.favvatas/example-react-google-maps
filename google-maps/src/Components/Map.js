import React from 'react';
// import Stack from 'react-bootstrap/Stack';
import GoogleMapContainer from './GoogleMapContainer'
import { Container, Row, Col } from 'react-bootstrap';


const Map = () => {
    return ( 
        <Container fluid className="h-100">
            <Row className="h-100">
                <Col className="p-0 position-relative">
                <div id="map" style={{ height: '100%', width: '100%' }}>
                    <GoogleMapContainer/>
                </div>
                </Col>
                {/* Other columns in the stack */}
            </Row>
            </Container>
    )
    // return ( 
    // <Stack gap={3}>
    //   <div><GoogleMapContainer/></div>
    //   {/* <div className="p-2">Second item</div> */}
    // </Stack>
    // )
}

export default Map