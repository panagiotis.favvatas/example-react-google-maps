// import React from 'react';
// import { GoogleMap, useJsApiLoader, Autocomplete } from '@react-google-maps/api';
// import { makeStyles } from '@material-ui/core/styles';
// import InputBase from '@material-ui/core/InputBase';
// import Box from '@material-ui/core/Box';
// import Grid from '@material-ui/core/Grid';
// // import Container from '@material-ui/core/Container';
// import Stack from '@material-ui/core/Grid';
// import Button from '@material-ui/core/Button';
// import Divider from '@material-ui/core/Divider';
// import SwipeableDrawer from '@material-ui/core/SwipeableDrawer';
// import Paper from '@material-ui/core/Paper';
// import IconButton from '@material-ui/core/IconButton';
// import SearchIcon from '@material-ui/icons/Search';
// import CircularProgress from '@material-ui/core/CircularProgress';
// import List from '@material-ui/core/List';
// import ListItem from '@material-ui/core/ListItem';
// import ListItemText from '@material-ui/core/ListItemText';
// import { Container } from '@material-ui/core';
// // import { IconMenu } from './IconMenu';

// const menuItems = ["Home", "About", "Contact", "Services", "Products"];

// const useStyles = makeStyles((theme) => ({
//   overlay: {
//     // position: 'absolute',
//     // top: '10px',
//     // left: '10px',
//     // zIndex: 2,
//   },
//   root: {
//     padding: '2px 4px',
//     display: 'flex',
//     alignItems: 'center',
//     width: '20%',
//     position: 'absolute',
//     top: '10px',
//     left: '10px',
//     zIndex: '999'
//   },
//   input: {
//     // marginLeft: theme.spacing(1),
//     // flex: 1,
//   },
//   iconButton: {
//     // padding: 10,
//   },
// }));

// const buttonStyle = {
//   backgroundColor: 'white',
//   borderRadius: 10,
// };

// const containerStyle = {
//   width: '100%',
//   height: '100vh'
// };

// const defaultCenter = {
//   lat: 34.0522, 
//   lng: -118.2437
// };

// const mapOptions = {
//   disableDefaultUI: true,
//   zoomControl: true,
//   fullscreenControl: true,
//   mapTypeId: 'roadmap' // 'roadmap', 'satellite', 'hybrid', 'terrain'
// };

// const GOOGLE_API_KEY = "AIzaSyCcrSI5N1E60E8NTZhsoi0Z61wS7om8wnQ";


// const GoogleMapContainer = () => {

//   const classes = useStyles();

//   const [drawerOpen, setDrawerOpen] = React.useState(false);
//   const toggleDrawer = (open) => (event) => {
//     if (event && event.type === 'keydown' && (event.key === 'Tab' || event.key === 'Shift')) {
//       return;
//     }
//     setDrawerOpen(open);
//   };
  


//   const autoCompleteRef = React.useRef(null);
//   const [selectedPlace, setSelectedPlace] = React.useState(null);

//   const { isLoaded, loadError } = useJsApiLoader({
//     id: 'google-map-script',
//     libraries: ['places'],
//     googleMapsApiKey: GOOGLE_API_KEY
//   })


//   const renderMap = () => {
    
//     return <GoogleMap
//       options={mapOptions}
//       mapContainerStyle={containerStyle}
//       zoom={10}
//       center={selectedPlace ? selectedPlace : defaultCenter}
//     >
//       {
//         // ...Your map components
//       }
//     </GoogleMap>
//   }

//   const renderHeader = () => {
    
//     return (
//       <Stack 
//         direction={{ xs: 'column', sm: 'row' }}
//         spacing={{ xs: 1, sm: 2, md: 4 }}
//         className={classes.root}
//       >
//         <Grid container spacing={2}>
//           <Grid item xs>
//             <Button 
//               onClick={toggleDrawer(true)}
//               style={buttonStyle}
//             >
//               Menu
//             </Button>
//           </Grid>
//           <Grid item xs>
//             <div>
//             </div>
//           </Grid>
//           <Grid item xs>test</Grid>
//         </Grid>
//       </Stack>
//     )
//   }
//   const renderLeftMenu = () => {
    
//     return (
//       <SwipeableDrawer
//           anchor="left"
//           open={drawerOpen}
//           onClose={toggleDrawer(false)}
//           onOpen={toggleDrawer(true)}
//         >
//           {/* <IconMenu/> */}
//         </SwipeableDrawer>
//     )
//   }

//   if (loadError) {
//     return <div>Map cannot be loaded right now, sorry.</div>
//   }

//   return (
//     <Box 
//       // component="span" 
//       sx={{ 
//         // p: 1, 
//         // border: '1px dashed grey' 
//       }}
//     >
//       {isLoaded ? 
//       <div>
//         {renderHeader()}
//         {renderLeftMenu()}
//         {renderMap()}
//       </div>
//       : <CircularProgress />
//       }
//     </Box>
//   )
// }

// export default React.memo(GoogleMapContainer)
